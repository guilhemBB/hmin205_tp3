package com.example.tp3e1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AffichageActivity extends AppCompatActivity {

    private LinearLayout myLayout2;
    String fileContent = "";
    FileInputStream fis;
    ArrayList<String> listeInfo = new ArrayList<>();
    String cpt="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage);
        this.myLayout2 = (LinearLayout) findViewById(R.id.layoutDinamique2);


        String[] fileList = this.fileList();
        for(int i=0; i<fileList.length; i++){
            //tot+=fileList[i]+"\n";
            fileContent = "";
            try {
                fis = openFileInput(fileList[i]);
                byte[] input = new byte[fis.available()];
                while (fis.read(input) != -1) {
                }
                fileContent += new String(input);
            }
            catch (FileNotFoundException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            if( !fileList[i].equals("fileCpt") && !fileList[i].equals("planning")) { //on n'affiche pas le compteur ni le planning dans la liste des donnée utiisateur
                listeInfo.add(fileContent);
            }
            else if(fileList[i].equals("fileCpt")){//et on affiche le cpt au début
                cpt=fileContent;
            }

            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        TextView myTV = new TextView(this);
        myTV.setText(cpt);
        myLayout2.addView(myTV);

        ListView lv = new ListView(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,listeInfo);
        lv.setAdapter(arrayAdapter);
        myLayout2.addView(lv);

        }



    }
