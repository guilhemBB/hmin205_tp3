package com.example.tp3e1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DBPlanning {
    @PrimaryKey
    public int id;

    @ColumnInfo(name = "creneau")
    public String creneau;

    @ColumnInfo(name="action")
    public String action;

    public DBPlanning(int id, String creneau, String action) {
        this.id = id;
        this.creneau = creneau;
        this.action = action;
    }
}
