package com.example.tp3e1;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

public class Utilisation implements LifecycleObserver {
    public static int cptUtilisation = 0;

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void NombreUtilisation() {
        cptUtilisation++;
    }
}
