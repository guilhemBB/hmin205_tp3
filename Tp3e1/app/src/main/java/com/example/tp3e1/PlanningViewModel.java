package com.example.tp3e1;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PlanningViewModel extends ViewModel {
    private MutableLiveData<String> MLtexteCreneau1;
    private MutableLiveData<String> MLtexteCreneau2;
    private MutableLiveData<String> MLtexteCreneau3;
    private MutableLiveData<String> MLtexteCreneau4;

  /*  public String texteCreneau1 = "Rencontre client Dupont";
    public String texteCreneau2 = "Travailler le dossier recrutement";
    public String texteCreneau3 = "Réunion équipe";
    public String texteCreneau4 = "Préparation dossier vente";*/

    public MutableLiveData<String> getMLtexteCreneau1() {
        if(MLtexteCreneau1 == null){
            MLtexteCreneau1 = new MutableLiveData<String>();
        }
        return MLtexteCreneau1;
    }

    public void setMLtexteCreneau1(String data) {
        MLtexteCreneau1.setValue(data);
    }

    public MutableLiveData<String> getMLtexteCreneau2() {
        if(MLtexteCreneau2 == null){
            MLtexteCreneau2 = new MutableLiveData<String>();
        }
        return MLtexteCreneau2;
    }

    public void setMLtexteCreneau2(String data) {
        MLtexteCreneau2.setValue(data);
    }

    public MutableLiveData<String> getMLtexteCreneau3() {
        if(MLtexteCreneau3 == null){
            MLtexteCreneau3 = new MutableLiveData<String>();
        }
        return MLtexteCreneau3;
    }

    public void setMLtexteCreneau3(String data) {
        MLtexteCreneau3.setValue(data);
    }

    public MutableLiveData<String> getMLtexteCreneau4() {
        if(MLtexteCreneau4 == null){
            MLtexteCreneau4 = new MutableLiveData<String>();
        }
        return MLtexteCreneau4;
    }

    public void setMLtexteCreneau4(String data) {
        MLtexteCreneau4.setValue(data);
    }
}
