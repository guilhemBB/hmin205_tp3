package com.example.tp3e1;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {DBPlanning.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PlanningDao planningDao();
}
