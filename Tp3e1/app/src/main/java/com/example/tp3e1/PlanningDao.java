package com.example.tp3e1;

        import androidx.room.Dao;
        import androidx.room.Delete;
        import androidx.room.Insert;
        import androidx.room.Query;

        import java.util.List;

@Dao
public interface PlanningDao {
    @Query("SELECT * FROM DBPlanning")
    List<DBPlanning> getAll();

    @Query("SELECT * FROM DBPlanning WHERE id (:planningId)")
    List<DBPlanning> loadAllByIds(int[] planningId);

    @Query("SELECT * FROM DBPlanning WHERE creneau LIKE (:creneau) LIMIT 1")
    DBPlanning findByCreneau(String creneau);

    @Insert
    void insertAll(DBPlanning... dbPlanning);

    @Insert
    void insert(DBPlanning dbPlanning);

    @Delete
    void delete(DBPlanning dbPlanning);

}
