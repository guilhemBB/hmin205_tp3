package com.example.tp3e1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;


import afu.org.checkerframework.checker.nullness.qual.Nullable;

public class Planning extends AppCompatActivity {

    private PlanningViewModel modelCreneau1;
    private PlanningViewModel modelCreneau2;
    private PlanningViewModel modelCreneau3;
    private PlanningViewModel modelCreneau4;
    private TextView tvCourrant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "planning").build();
        PlanningDao planningDao = db.planningDao();
        planningDao.insert(new DBPlanning(0,"08h", "Rencontre client Dupont"));
        planningDao.insert(new DBPlanning(1,"10h", "Travailler le dossier recrutement"));
        planningDao.insert(new DBPlanning(2,"12h", "Réunion équipe"));
        planningDao.insert(new DBPlanning(3,"14h", "Préparation dossier vente"));

        // Get the ViewModel.
        modelCreneau1 = new ViewModelProvider(this).get(PlanningViewModel.class);
        modelCreneau2 = new ViewModelProvider(this).get(PlanningViewModel.class);
        modelCreneau3 = new ViewModelProvider(this).get(PlanningViewModel.class);
        modelCreneau4 = new ViewModelProvider(this).get(PlanningViewModel.class);

        // Create the observer which updates the UI.
        final Observer<String> creneau1_Observer = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                tvCourrant = findViewById(R.id.tvCr1);
                tvCourrant.setText(newName);
            }
        };

        final Observer<String> creneau2_Observer = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                tvCourrant = findViewById(R.id.tvCr2);
                tvCourrant.setText(newName);
            }
        };

        final Observer<String> creneau3_Observer = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                tvCourrant = findViewById(R.id.tvCr3);
                tvCourrant.setText(newName);
            }
        };

        final Observer<String> creneau4_Observer = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newName) {
                // Update the UI, in this case, a TextView.
                tvCourrant = findViewById(R.id.tvCr4);
                tvCourrant.setText(newName);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        modelCreneau1.getMLtexteCreneau1().observe(this, creneau1_Observer);
        modelCreneau2.getMLtexteCreneau2().observe(this, creneau2_Observer);
        modelCreneau3.getMLtexteCreneau3().observe(this, creneau3_Observer);
        modelCreneau4.getMLtexteCreneau4().observe(this, creneau4_Observer);

        modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("08h").action);
        modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("10h").action);
        modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("12h").action);
        modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("14h").action);


        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                int msInADay = 3600 * 24 * 1000;
                modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("08h").action);
                modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("10h").action);
                modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("12h").action);
                modelCreneau1.setMLtexteCreneau1(planningDao.findByCreneau("14h").action);

                handler.postDelayed(this, 1000);//msInADay
            }
        });

    }

}