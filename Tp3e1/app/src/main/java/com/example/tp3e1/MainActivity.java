package com.example.tp3e1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;


public class MainActivity extends AppCompatActivity {
    EditText etCourrant;
    TextView tvCourrant;
    String stCourrant;
    private Random random = new Random();
    private String idRandom = "";
    Button soumettre;
    Button affichierPlanning;
    private String fileName = "";
    private String dataBackup = "";
    String nbUtil = "";

     String name = "";
     String firstname = "";
     String age = "";
     String phone = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getLifecycle().addObserver(new Utilisation());

        if(savedInstanceState != null){
            name = savedInstanceState.getString("name");
            etCourrant = findViewById(R.id.ETname);
            etCourrant.setText(name);

            stCourrant = savedInstanceState.getString("firstname");
            etCourrant = findViewById(R.id.ETfirstName);
            etCourrant.setText(stCourrant);

            stCourrant = savedInstanceState.getString("age");
            etCourrant = findViewById(R.id.ETage);
            etCourrant.setText(stCourrant);

            stCourrant = savedInstanceState.getString("phone");
            etCourrant = findViewById(R.id.ETphone);
            etCourrant.setText(stCourrant);

            idRandom = savedInstanceState.getString("idRandom");
        }else{
            randomIdGen();
            tvCourrant = findViewById(R.id.tvIdRandom);
            tvCourrant.setText(idRandom);
        }


        soumettre = findViewById(R.id.ButtonSubmit);
        soumettre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCourrant=findViewById(R.id.ETname);
                name=etCourrant.getText().toString();
                etCourrant=findViewById(R.id.ETfirstName);
                firstname=etCourrant.getText().toString();
                etCourrant=findViewById(R.id.ETage);
                age=etCourrant.getText().toString();
                etCourrant=findViewById(R.id.ETphone);
                phone=etCourrant.getText().toString();

                fileName = name+idRandom;
                dataBackup = name+";"+firstname+";"+age+";"+phone+"\n" ;
                FileOutputStream fos = null;
                FileOutputStream fosCpt = null;
                try {
                    fos = openFileOutput(fileName, Context.MODE_PRIVATE);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    fos.write(dataBackup.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try{
                    fosCpt = openFileOutput("fileCpt", Context.MODE_PRIVATE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    nbUtil="Utilisation n°"+Utilisation.cptUtilisation;
                    fosCpt.write(nbUtil.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Intent myIntentSoumettre= new Intent(MainActivity.this, AffichageActivity.class);
                startActivity(myIntentSoumettre);
            }
        });

        affichierPlanning = findViewById(R.id.buttonPlanning);
        affichierPlanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPlanning= new Intent(MainActivity.this, Planning.class);
                startActivity(intentPlanning);
            }
        });

    }

    private void randomIdGen(){
        idRandom = String.valueOf(random.nextDouble());
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        etCourrant = findViewById(R.id.ETname);
        savedInstanceState.putString("name", etCourrant.getText().toString());
        etCourrant = findViewById(R.id.ETfirstName);
        savedInstanceState.putString("firstname",etCourrant.getText().toString());
        etCourrant = findViewById(R.id.ETage);
        savedInstanceState.putString("age", etCourrant.getText().toString());
        etCourrant = findViewById(R.id.ETphone);
        savedInstanceState.putString("phone",etCourrant.getText().toString());

        tvCourrant = findViewById(R.id.tvIdRandom);
        idRandom = tvCourrant.getText().toString();
        savedInstanceState.putString("idRandom", idRandom);

    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            stCourrant = savedInstanceState.getString("name");
            etCourrant = findViewById(R.id.ETname);

            stCourrant = savedInstanceState.getString("firstname");
            etCourrant = findViewById(R.id.ETfirstName);

            stCourrant = savedInstanceState.getString("age");
            etCourrant = findViewById(R.id.ETage);

            stCourrant = savedInstanceState.getString("phone");
            etCourrant = findViewById(R.id.ETphone);

            tvCourrant = findViewById(R.id.tvIdRandom);
            tvCourrant.setText(idRandom);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getLifecycle().removeObserver(new Utilisation());

    }

}
